*Give a shot Normal.CSS in the event that you need your site with a straightforward layout.*

# Normal.CSSCool
Normal.CSSCool - A CSS system to make your web style typical.
## Introduce
Add this line to the 'head' part of your site:
```
<link rel="stylesheet" href="https://raw.githubusercontent.com/softorangetech200/Normal.CSSCool/0.1/Normal.CSS">
```
To deliver textual styles as Roboto add the accompanying credits in the 'head' area too:
```
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
```
## Permit
Authorized under MIT. [Read the full MIT License here](LICENSE.md).
