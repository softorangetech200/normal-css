# normal.css

normal.css - A CSS framework to make your web style normal.

![Normal css Logo](https://user-images.githubusercontent.com/111031232/185766129-42c09d25-1827-4dda-ac45-f4988b6b6b01.png)


## Install

Add this line to the `head` section of your website:

```
<link rel="stylesheet" href="https://raw.githubusercontent.com/softorangetech200/normal.css/0.1/normal.css">
```

### Needs credit on website?

Not needed, but if you want you can use the badge below (thanks shields.io!).<br><br>
[![The badge that you can use for projects if you like](https://img.shields.io/badge/Uses-Normal.CSS-yellow?style=for-the-badge)](github.com/softorangetech200/normal.css)
## License
Licensed under GPL-3.0. [Read the full GPL-3.0 License here](LICENSE.md).
