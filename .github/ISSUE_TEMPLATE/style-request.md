---
name: Style request
about: Request a style to be added
title: ''
labels: status-waiting, style-request
assignees: ''

---

**Describe the solution you'd like**
A clear and concise description of what you want to happen.

**Additional context** 
Add any other context or screenshots about the style request here.
<!-- IMPORTANT: The Normal.CSSCool issue tracker is separate from Normal.CSS issue tracker. Please report Normal.CSSCool issues on its issue tracker. -->
